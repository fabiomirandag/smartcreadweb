$(function() {
    $('#easypiechart-vendas').easyPieChart({
        scaleColor: false,
        barColor: '#000000'
    });
});

$(function() {
    $('#easypiechart-divida').easyPieChart({
        scaleColor: false,
        barColor: '#000000'
    });
});

$(function() {
    $('#easypiechart-red').easyPieChart({
        scaleColor: false,
        barColor: '#30a5ff'
    });
});

$(function() {
   $('#easypiechart-blue').easyPieChart({
       scaleColor: false,
       barColor: '#30a5ff'
   });
});

$(function() {
   $('#easypiechart-green').easyPieChart({
       scaleColor: false,
       barColor: '#30a5ff'
   });
});

$('.chart').easyPieChart({
     scaleColor: false,
      barColor: '#30a5ff'
});

